import java.util.*;
import java.io.*;

//user data first 500 values are for enrolment phase of system
//rest for verification phase
//threshold is 71

public class main {
  
  private static final int MAX_X = 3000;
  private static final int MAX_Y = 5;
  
  public static void main(String[] args) throws java.io.IOException {
    
    System.out.println("Initializing...");
    Scanner scan = new Scanner(System.in);
    
    System.out.print("Enter userID: ");
    String userId = scan.next().toLowerCase();
    int user = 0;
    
    int[][] data1 = new int[MAX_X][MAX_Y];
    int[][] data2 = new int[MAX_X][MAX_Y];
    int[][] data3 = new int[MAX_X][MAX_Y];
    int[][] data4 = new int[MAX_X][MAX_Y];
    int[][] data5 = new int[MAX_X][MAX_Y];
    
    //only 500 because we only need the first 500 to compare to everything else. 
    //saves space 
    int[][] primA = new int[500][5];
    
    //Access control List (5 Users, 6 File Ea.)
    int[][] accessCtrl = new int[5][6];
    
    int[] fR = new int[MAX_Y];
    int[] fA1 = new int[MAX_Y];
    int[] fA2 = new int[MAX_Y];
    int[] fA3 = new int[MAX_Y];
    int[] fA4 = new int[MAX_Y];
    int[] fA5 = new int[MAX_Y];
    double[] devArr1 = new double[MAX_Y];
    double[] devArr2 = new double[MAX_Y];
    double[] devArr3 = new double[MAX_Y];
    double[] devArr4 = new double[MAX_Y];
    double[] devArr5 = new double[MAX_Y];
    double far = 0;
    double frr = 0;
    
    Reader reader = new Reader();
    
    reader.readArray("Lab2-Sample-Files/User1.txt", data1, 1);
    reader.readArray("Lab2-Sample-Files/User2.txt", data2, 1);
    reader.readArray("Lab2-Sample-Files/User3.txt", data3, 1);
    reader.readArray("Lab2-Sample-Files/User4.txt", data4, 1);
    reader.readArray("Lab2-Sample-Files/User5.txt", data5, 1);
    
    /*int s = 2, n = 500, x = 5;
     int index = (s-1)*n + (x-1);
     System.out.println(data4[index][0]);
     Gives Record X from txt of section S with section length of N
     */
    
    switch (userId) {
      case "user1":
        System.out.println("Setting up User1 as Primary");
        reader.transferIt(primA, data1);
        devArr1 = dev(primA, data1);
        devArr2 = dev(primA, data2);
        devArr3 = dev(primA, data3);
        devArr4 = dev(primA, data4);
        devArr5 = dev(primA, data5);
        fR = fRThresh(devArr1, 75.0);
        fA1 = fAThresh(devArr2, 81.0);
        fA2 = fAThresh(devArr3, 82.0);
        fA3 = fAThresh(devArr4, 85.0);
        fA4 = fAThresh(devArr5, 88.0);
        frr = getFrr(fR);
        far = getFar(fA1, fA2, fA3, fA4);
        user = 1;
        break;
      case "user2":
        System.out.println("Setting up User2 as Primary");
        reader.transferIt(primA, data2);
        devArr1 = dev(primA, data2);
        devArr2 = dev(primA, data1);
        devArr3 = dev(primA, data3);
        devArr4 = dev(primA, data4);
        devArr5 = dev(primA, data5);
        fR = fRThresh(devArr1, 71.0);
        fA1 = fAThresh(devArr2, 75.0);
        fA2 = fAThresh(devArr3, 80.0);
        fA3 = fAThresh(devArr4, 82.0);
        fA4 = fAThresh(devArr5, 77.0);
        frr = getFrr(fR);
        far = getFar(fA1, fA2, fA3, fA4);
        user = 2;
        break;
      case "user3":
        System.out.println("Setting up User3 as Primary");
        reader.transferIt(primA, data3);
        devArr1 = dev(primA, data3);
        devArr2 = dev(primA, data2);
        devArr3 = dev(primA, data1);
        devArr4 = dev(primA, data4);
        devArr5 = dev(primA, data5);
        fR = fRThresh(devArr1, 71.0);
        fA1 = fAThresh(devArr2, 80.0);
        fA2 = fAThresh(devArr3, 75.0);
        fA3 = fAThresh(devArr4, 72.0);
        fA4 = fAThresh(devArr5, 80.0);
        frr = getFrr(fR);
        far = getFar(fA1, fA2, fA3, fA4);
        user = 3;
        break;
      case "user4":
        System.out.println("Setting up User4 as Primary");
        reader.transferIt(primA, data4);
        devArr1 = dev(primA, data4);
        devArr2 = dev(primA, data2);
        devArr3 = dev(primA, data3);
        devArr4 = dev(primA, data1);
        devArr5 = dev(primA, data5);
        fR = fRThresh(devArr1, 78.0);
        fA1 = fAThresh(devArr2, 71.0);
        fA2 = fAThresh(devArr3, 76.0);
        fA3 = fAThresh(devArr4, 75.0);
        fA4 = fAThresh(devArr5, 80.0);
        frr = getFrr(fR);
        far = getFar(fA1, fA2, fA3, fA4);
        user = 4;
        break;
      case "user5":
        System.out.println("Setting up User5 as Primary");
        reader.transferIt(primA, data5);
        devArr1 = dev(primA, data5);
        devArr2 = dev(primA, data2);
        devArr3 = dev(primA, data3);
        devArr4 = dev(primA, data4);
        devArr5 = dev(primA, data1);
        fR = fRThresh(devArr1, 71.0);
        fA1 = fAThresh(devArr2, 85.0);
        fA2 = fAThresh(devArr3, 80.0);
        fA3 = fAThresh(devArr4, 85.0);
        fA4 = fAThresh(devArr5, 85.0);
        frr = getFrr(fR);
        far = getFar(fA1, fA2, fA3, fA4);
        user = 5;
        break;
      default:
        System.out.println("Error reading UserID!");
        System.exit(0);
    }
    /*
     System.out.println(Arrays.toString(fR));
     System.out.printf("FRR is: %.03f\n",frr);
     System.out.printf("FAR is: %.03f\n",far);
     */
    if (far == frr) {
      System.out.println("Access Granted!");
    } else
      System.out.println("Access Failed!");
    
    //reads Default matrix
    //reader.initACL(accessCtrl, true);
    
    //reads Modified matrix
    reader.initACL(accessCtrl, false);
    
    Role sales = new Role("Sales", 5); //rx
    Role techStaff = new Role("Technical Staff", 7); //rwx
    Role manager = new Role("Manager", 7); //rwx
    
    User user1 = new User(techStaff);
    User user2 = new User(manager);
    User user3 = new User(sales);
    User user4 = new User(sales);
    User user5 = new User(sales);
    
    System.out.println("Enter file you want to access: ");
    String file = scan.next().toLowerCase();

    System.out.println("Enter file access request: ");
    String fileReq = scan.next();
    
    System.out.println("ACL based : ");
    System.out.println(reader.getAccessACL(file, user, fileReq, accessCtrl));
    
    System.out.println("RACL based : ");
    switch(user){
      case 1:
        System.out.println("User Role is " + user1.getRoleName() + " with " + reader.getAccess(user1.getAccessCode()) + " access.");
        System.out.println(user1.getAccessReq(file, fileReq));
        break;
      case 2:
        System.out.println("User Role is " + user2.getRoleName() + " with " + reader.getAccess(user2.getAccessCode()) + " access.");
        System.out.println(user2.getAccessReq(file, fileReq));
        break;
      case 3:
        System.out.println("User Role is " + user3.getRoleName() + " with " + reader.getAccess(user3.getAccessCode()) + " access.");
        System.out.println(user3.getAccessReq(file, fileReq));
        break;
      case 4:
        System.out.println("User Role is " + user4.getRoleName() + " with " + reader.getAccess(user4.getAccessCode()) + " access.");
        System.out.println(user4.getAccessReq(file, fileReq));
        break;
      case 5:
        System.out.println("User Role is " + user5.getRoleName() + " with " + reader.getAccess(user5.getAccessCode()) + " access.");
        System.out.println(user5.getAccessReq(file, fileReq));
        break;
    }
  }
  
  /**
   * Gets the deviation value by comparing two arrays 
   * @param prim the primary array that is used to compare to other array
   * @param compare the array that is compared to primary array
   * @return double[] returns a double array that holds all deviation values
   */
  public static double[] dev(int[][] prim, int[][] compare) {
    double totalD, totalM, totalM1, d, xD, xM1, xM2, n1, n2, dval;
    n1 = 500;
    n2 = n1 - 1;
    totalD = totalM1 = 0;
    double[] devArr = new double[5];
    
    for (int y = 2; y < 7; y++) {
      for (int i = 0; i < prim.length; i++) {
        int index = 500 * (y - 1) + (i - 1);
        xD = Math.abs(prim[i][2] - compare[index][2]);
        xD = xD / (compare[index][2]);
        totalD += xD;
        if (i < (prim.length - 1)) {
          xM1 = Math.abs((prim[i][3]) - (compare[index][3]));
          xM2 = xM1 / (compare[index][3]);
          totalM1 += xM2;
        }
      }
      d = totalD * (1 / n2);
      totalM = totalM1 * (1 / n1);
      dval = (totalM + d) * 50;
      devArr[y - 2] = dval;
      totalD = totalM = dval = d = xD = xM1 = xM2 = totalM1 = 0;
    }
    
    return devArr;
  }
  /**
   * Compares deviation value with threshold in regards to FR requirements 
   * @param der the array that holds all the deviation values
   * @param thr the threshold determined to get FR values
   * @return int [] returns the int array that holds all FR values
   */
  public static int[] fRThresh(double[] der, double thr) {
    int fr = 0;
    int[] frArr = new int[5];
    // if D>= Thr then, FR=1 else FR=0;
    for (int i = 0; i < der.length; i++) {
      if (der[i] >= thr) {
        fr = 1;
      } else {
        fr = 0;
      }
      frArr[i] = fr;
    }
    return frArr;
  }
  /**
   * Compares deviation value with threshold in regards to FA requirements
   * @param der the array that holds all deviation values
   * @param thr the threshold determined to get FA values
   * @return int [] returns the int array that holds all FA values
   */
  public static int[] fAThresh(double[] der, double thr) {
    int fa = 0;
    int[] faArr = new int[5];
    // if D<= Thr then, FA=1 else FA=0;
    for (int i = 0; i < der.length; i++) {
      if (der[i] <= thr) {
        fa = 1;
      } else {
        fa = 0;
      }
      faArr[i] = fa;
    }
    return faArr;
  }
  public static double getFrr(int[] f) {
    double total = 0;
    for (int i = 0; i < f.length; i++) {
      total += f[i];
    }
    total = total / f.length;
    return total;
  }
  /**
   * Gets the FA values of several arrays and combines them to get FAR
   * @param f1 first array to calculate far value
   * @param f2 second array to calculate far value
   * @param f3 third array to calculate far value
   * @param f4 fourth array to calculate far value
   * @return double Returns the FAR value
   */
  public static double getFar(int[] f1, int[] f2, int[] f3, int[] f4) {
    int[] a = new int[20];
    double total = 0;
    for (int i = 0; i < 5; i++) {
      a[i] = f1[i];
      a[i + 5] = f2[i];
      a[i + 10] = f3[i];
      a[i + 15] = f4[i];
      total += f1[i] + f2[i] + f3[i] + f4[i];
    }
    total = total / a.length;
    return total;
  }
}
