import java.util.*;
import java.io.*;

public class Reader {
  
   /**
   * Fills array with userdata from text file. 
   * @param filename the file that is being read from
   * @param array the array which is being filled with data from textfile
   * @param type always 1
   * @return 1 is sucessfully filled 0 if error reading data.
   */
  public int readArray(String filename, int array[][], int type) {
    System.out.println("Reading file : " + filename);
    try{
      File f = new File(filename);
      Scanner s = new Scanner(f);
      for (int i = 0; i < array.length; i++) {
        for (int j = 0; j < array[i].length; j++) {
          if(type == 1) {
            array[i][j] = s.nextInt(); 
          }
          else {
            array[i][j] = getAccess(s.next());
            //System.out.println(array[i][j]);
          }
        }
      }
    }
    catch(Exception e){ 
      System.out.println("Error reading data : " + e);
      return 0;
    }
    return 1;
  }
  
   /*Takes care of data transfering. Primarily used in copying UserX main 500 data to
   * another array for easier access and comparing
   */
    public int transferIt(int[][] src, int[][] dst) {
    for (int i = 0; i < src.length; i++) {
      for (int j = 0; j < src[i].length; j++) {
        src[i][j]=dst[i][j];
      }
    }
    return 1;
  }
  
  //Obtains permissions user has access to using param accessCode.
  public String getAccess(int accessCode) {
    switch(accessCode) {
      case 0:
        return "No Access";
      case 1:
        return "x";
      case 2:
        return "w";
      case 3:
        return "wx";
      case 4:
        return "r";
      case 5:
        return "rx";
      case 6:
        return "rw";
      case 7:
        return "rwx";
      default:
        return "Invalid Access Code";
    }
  }
  
  //Obtains access code from checking array's entry.
  public int getAccess(String access) {
    switch(access) {
      case "x":
        return 1;
      case "w":
        return 2;
      case "wx":
        return 3;
      case "r":
        return 4;
      case "rx":
        return 5;
      case "rw":
        return 6;
      case "rwx":
        return 7;
      default:
        return 0;
    }
  }
  
  public String getAccessACL(String file, int user, String fileReq, int[][] access) {
    int fileNum = -1;
    switch(file) {
      case "file1":
        fileNum = 1;
        break;
      case "file2":
        fileNum = 2;
        break;
      case "file3":
        fileNum = 3;
        break;
      case "file4":
        fileNum = 4;
        break;
      case "file5":
        fileNum = 5;
        break;
      default:
        System.out.println("Invalid file name!");
        System.exit(0);
    }
    if(getAccess(access[user-1][fileNum-1]).contains(fileReq))
      return "Your " + fileReq + " access request to " + file + " is granted.";
    else
      return "You only have " + getAccess(access[user-1][fileNum-1]) + " access rights to " + file;
  }
  
  /*fills 5 arrays with flags to their access rights to files*/
  public int initACL(int[][] access, boolean flag) {
    
    /*
     * 0 = no access
     * 4 = read
     * 2 = write
     * 1 = execute
     * 
     * Example : rw = 4+2 = 6
     *           rwx = 4+2+1 = 7
     */
    
    /* Default Matrix : 
     *      F1  F2  F3  F4  F5  F6
     * U1 : rwx rwx rwx rwx rwx rwx
     * U2 : rw      r x     r
     * U3 :         rw  rw
     * U4 : r x         r   r
     * U5 :     rwx     r       r
     */
    
    if(flag) {
      System.out.println("Initalizing ACL - Default");
      //Set up User1
      for(int x = 0; x < 6; x ++)
        access[0][x] = 7;
      
      //Set up User2
      access[1][0] = 6;
      access[1][1] = 1;
      access[1][2] = 5;
      access[1][3] = 5;
      access[1][4] = 1;
      access[1][5] = 1;
      
      //Set up User3
      access[2][0] = 1;
      access[2][1] = 1;
      access[2][2] = 6;
      access[2][3] = 6;
      access[2][4] = 1;
      access[2][5] = 1;
      
      //Set up User4
      access[3][0] = 5;
      access[3][1] = 1;
      access[3][2] = 1;
      access[3][3] = 4;
      access[3][4] = 4;
      access[3][5] = 1;
      
      //Set up User5
      access[4][0] = 1;
      access[4][1] = 7;
      access[4][2] = 1;
      access[4][3] = 4;
      access[4][4] = 1;
      access[4][5] = 4;
    }
    else {
      System.out.println("Initalizing ACL - Modified");
      readArray("ACL.txt", access, 0);
    }
    
    return 1;
  }
}
