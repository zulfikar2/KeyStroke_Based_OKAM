/*Use for 3 roles : Sales, Technical Staff, Manager*/
import java.util.*;
import java.io.*;

public class User{
  
  Role role;
  
  //Assigns users to a role.
  User(Role role){
    this.role = role;
  }
  
  //Gets accesscode(permissions) of role.
  int getAccessCode() {
    return role.getAccessCode();
  }
  
  //Gets current role.
  Role getRole() {
    return role;
  }
  
  //Gets name of role.
  String getRoleName() {
    return role.getRole();
  }
  
  //Sets user to a certain role.
  void setRole(Role role) {
    this.role = role;
  }
  
  
   /**
   * Obtains what the user can access. 
   * @param file the file that is being accessed. 
   * @param fileReq the permissions the user or role has.
   * @return access granted if contains fileReq.
   * @return access failed if does not contain fileReq.
   */
  String getAccessReq(String file, String fileReq) {
    Reader read = new Reader();
    if(read.getAccess(getAccessCode()).contains(fileReq)) {
      System.out.println(getRoleName());
      return "Your " + fileReq + " access request to " + file + " is granted.";
    }
    else
      return "You only have " + read.getAccess(getAccessCode()) + " access rights to " + file;
  }
}
