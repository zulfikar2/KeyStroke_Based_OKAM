import java.util.*;
import java.io.*;

public class Role {
  
  String role = "NULL";
  int access = 0;
  
  //Defines role objects.
  Role(String role, int access) {
    this.role = role;
    this.access = access;
  }
  
  //Gets access code of role.
  int getAccessCode() {
    return access;
  }
  
  //Gets the name of the role.
  String getRole() {
    return role;
  }
}
